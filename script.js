

window.onload = function () {
    const fashionArena = document.getElementById("fashionArena");
    const outfit = document.getElementById('outfit');
    const bgMusic = document.getElementById("bgMusic");
    const bubble = document.getElementById("bubbleSound");
    const click = document.getElementById("clickSound");
    const fairyDust = document.getElementById("fairyDustSound");
    const musicController = document.getElementById("music");
    let clothItems = document.getElementsByClassName("cloth-item");
    const model = document.querySelector("#model");

    let musicPlaying = true;

    let butterfly_purple = { id: "butterfly_purple", title: "Beautiful purple dress", type: "dress", color: 'purple', zIndex: 1, url: './images/odest_collection/buterfly-purple.png' };
    let butterfly_red = { id: "butterfly_red", title: "Beautiful red dress", type: "dress", color: 'purple', zIndex: 1, url: './images/odest_collection/butterfly-red.png' };
    let evi4 = { id: "evi4", title: "Beautiful tulle dress", type: "dress", color: 'purple', zIndex: 1, url: './images/odest_collection/evi4.png' };
    let tulle_skirt_beige = { id: "tulle_skirt_beige", title: "Beautiful tulle dress", type: "dress", color: 'purple', zIndex: 1, url: './images/odest_collection/tulle_skirt_beige.png' };
    let tulle_skirt_purple = { id: "tulle_skirt_purple", title: "Beautiful tulle dress", type: "dress", color: 'purple', zIndex: 1, url: './images/odest_collection/tulle_skirt_purple.png' };

    let wardrobe = { butterfly_purple, butterfly_red, evi4, tulle_skirt_beige, tulle_skirt_purple};

    fashionArena.onmousemove = (ev) => {
        // console.log(ev, 'ev');
    }

    clothItems = document.querySelectorAll('.cloth-item');

    let clickEvent = (ev) => {
        ev.stopPropagation();
        click.play();
        const item = ev.target.dataset.clothid;
        addNewCloth(wardrobe[item]);
    }
    let mouseOver = (ev) => {
        bubble.play();
    }

    clothItems.forEach((item) => {
        item.addEventListener('click', clickEvent)
    });
    clothItems.forEach((item) => {
        item.addEventListener('mouseover', mouseOver)
    });



    function playMusic() {
        bgMusic.play();
        musicPlaying = true;

        // function pauseAudio() { 
        // x.pause(); 
        // } 
    }

    function stopMusic() {
        bgMusic.pause();
        musicPlaying = false;
    }

    playMusic();
    musicController.addEventListener('click', function () {
        if (musicPlaying) {
            stopMusic();
        } else {
            playMusic();
        }
    });

    function addNewCloth(item) {
        removeCloth(item);
        const image = document.createElement('img');
        modelHeight = model.clientHeight;

        image.setAttribute(
            'src',
            item.url,
        );
        image.setAttribute('data-clothid', item.id);
        image.setAttribute('data-category', item.type);
        image.setAttribute('id', item.id);
        image.style.position = 'absolute';

        image.onerror = function handleError() {
            console.log('Image could not be loaded');
            // 👇️ Can set image.src to a backup image here
            // image.src = 'backup-image.png'

            // 👇️ Or hide image
            // image.style.display = 'none';
        };

        outfit.appendChild(image);

    }

    function removeCloth(item) {
        let found = [].find.call(outfit.children, activeItem => {
            console.log(activeItem.dataset['category'])
            return wardrobe[item.id].type == activeItem.dataset['category']
        });

        found && outfit.removeChild(found);

        // found && [].forEach.call([found] ,el => {
        //     outfit.removeChild(el)
        // });
    }

    // addNewCloth(garment);

    //adding new clothing item




}


// the glitter mouse



